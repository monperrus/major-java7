package com.sun.tools.javac.mutation.operator;

import java.util.Collection;
import java.util.List;

/**
 * Interface for mutation providers
 */
public interface IMutationProvider {
    /**
     * The name of the command line option to enable/disable mutation operators
     */
    public static final String OPTION_NAME_MUTATOR = "-XMutator";

    // Type of mutation operator
    public enum MutationOperatorType{BIN, UNR, DEL, LIT; }

    // Enum of all available mutation operators
    public enum MutationOperator{AOR, LOR, SOR, COR, ROR, ORU, LVR, EVR, STD;
        /*
         * Return comma separated list of all mutation operators
         */
        public static String getOperatorList(){
            // String buffer for all options
            StringBuffer buf = new StringBuffer(64);
            // String buffer for disable options
            StringBuffer bufNeg = new StringBuffer(32);

            // Wildcard ALL does not have a disable option ->
            // add it to the beginning of the list
            buf.append("ALL,");

            for(MutationOperator op : MutationOperator.values()){
                // Add enable options
                buf.append(op.name());
                buf.append(',');
                // Add disable options
                bufNeg.append("-"+op.name());
                bufNeg.append(',');
            }
            // delete last comma of neg list
            bufNeg.deleteCharAt(bufNeg.length()-1);
            buf.append(bufNeg);

            return buf.toString();
        }
    }
    // Enum of all supported statements for the statement deletion operator
    public static enum STD_TYPE {CALL, INC, DEC, ASSIGN, CONT, BREAK, RETURN};

    // Enum of all supported literals for the literal value replacement operator
    public static enum LIT_TYPE {NUMBER, BOOLEAN, STRING};

    public Collection<String> getMutationOperators(List<String> flatname, MutationOperatorType kind, String sym);

    /**
     * Returns true iff the mutation operator <code>op</code> is enabled for the entity <code>flatname</code>
     */
    public boolean isOperatorEnabled(List<String> flatname, MutationOperator op);
}
