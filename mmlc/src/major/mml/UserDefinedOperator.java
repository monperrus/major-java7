package major.mml;

import java.util.ArrayList;
import java.util.List;

import com.sun.tools.javac.mutation.operator.TreeMutationProvider;

public class UserDefinedOperator implements Statement{
    private List<Statement> stmtList = new ArrayList<Statement>(32);
    private String ident;

    public UserDefinedOperator(String ident, List<Statement> stmtList){
        this.ident=ident;
        this.stmtList=stmtList;
    }

    public void addStmt(Statement stmt){
        stmtList.add(stmt);
    }

    @Override
    public boolean execute(TreeMutationProvider tree, List<String> flatname){
        if(stmtList.size()==0) return true;
        for(Statement stmt:stmtList){
            if(!stmt.execute(tree, flatname)) return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuffer buf=new StringBuffer(128);
        buf.append("UserDefOp: "+ident+"\n");
        for(Statement stmt:stmtList){
            buf.append("- "+stmt.toString());
        }
        return buf.toString();
    }
}
