package major.mml;

import java.util.List;
import java.util.Set;

import com.sun.tools.javac.mutation.operator.TreeMutationProvider;
import com.sun.tools.javac.mutation.operator.IMutationProvider.MutationOperatorType;

public class ReplacementStatement implements Statement {
    private MutationOperatorType type;
    private String sym;
    private Set<String> opList;
    private boolean replace;

    public ReplacementStatement(MutationOperatorType type, String sym, Set<String> opList, boolean replace){
        this.type=type;
        this.sym=sym;
        this.opList=opList;
        this.replace=replace;
    }

    @Override
    public boolean execute(TreeMutationProvider tree, List<String> flatname) {
        return tree.setMutationOperators(flatname, type, sym, opList, replace);
    }

    @Override
    public String toString() {
        String list="{";
        for(String str:opList){
            list+=(" "+str);
        }
        list+=" }";

        return "ReplaceStmt "+type+": "+sym+" -> "+list;
    }
}
