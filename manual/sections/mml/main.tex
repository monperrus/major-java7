\major is designed to support a wide variety of configurations by means of its
own domain specific language, called \mml.  Generally, a \mml script contains a
sequence of an arbitrary number of statements, where a statement represents one
of the following entities:
%
\begin{itemize*}
\item Variable definition %(\nonterm{vardef\_stmt})
\item Replacement definition %(\nonterm{replace\_stmt})
\item Definition of statement types for the \op{STD} operator %(\nonterm{delete\_stmt})
\item Definition of literal types for the \op{LVR} operator
\item Definition of a mutation operator group %(\nonterm{opdef\_stmt})
\item Invocation of a mutation operator (group)%(\nonterm{call\_stmt})
\item Line comment %(\term{LINE\_COMMENT})
\end{itemize*}
While the first five statements are terminated by a semicolon, an operator
definition is encapsulated by curly braces and a line comment is terminated by
the end-of-line.

\section{Statement scopes}
\mml provides statement scopes for replacement definitions and operator 
invocations to support the mutation of a certain package, class, or method 
within a program. Figure~\ref{fig:mml/scope} depicts the definition of a
statement scope, which can cover software units at different levels of
granularity --- from a specific method up to an entire package.  Note that a
statement scope is optional as indicated by the first rule of
Figure~\ref{fig:mml/scope}.  If no statement scope is provided, the
corresponding replacement definition or operator call is applied to the root
package.
%
The scope's corresponding entity, that is package, class, or method, is 
determined by means of its fully qualified name, which is referred to as 
flatname.  Such a flatname can be either provided within delimiters 
(quotes) or by means of a variable identified by \term{IDENT}.

\begin{figure}
\begin{center}
    \input{images/figureScope}
    \caption{Syntax diagram for the definition of a statement scope.}
    \label{fig:mml/scope}
\end{center}
\end{figure}

\begin{figure}
\begin{center}
    \input{images/figureFlat}
    \caption{Syntax diagram for the definition of a flatname.}
    \label{fig:mml/flat}
\end{center}
\end{figure}
Figure~\ref{fig:mml/flat} shows the grammar rules for assembling a flatname.
The naming conventions for valid identifiers
(\term{IDENT}) are based on those of the Java programming language due to the
fact that a flatname identifies a certain entity within a Java program. The
following four examples show valid flatnames for a package,
a class, a set of overloaded methods, and a particular method:
%
\begin{itemize*} 
    \item \code{"java.lang"}
    \item \code{"java.lang.String"}
    \item \code{"java.lang.String@substring"}
    \item \code{"java.lang.String::substring(int,int)"}
\end{itemize*}
%
Note that the syntax definition of a flatname also supports the identification
of innerclasses and constructors, consistent with the naming conventions of
Java. For Example, the subsequent definitions address an inner class, a
constructor, and a static class initializer:  
%
\begin{itemize*} 
    \item \code{"foo.Bar\$InnerClass"} 
    \item \code{"foo.Bar@<init>"} 
    \item \code{"foo.Bar@<clinit>"} 
\end{itemize*} 
%

\section{Overriding and extending definitions} 
In principle, mutation operators can be enabled (\verb|+|), which is the default
if the flag is omitted, or disabled (\verb|-|) and this behavior can be defined
for each scope. In the following example, the AOR mutation operator is generally
enabled for the package \verb|org| but, at the same time, disabled for the class
\verb|Foo| within this package:
%
\begin{Verbatim}[baselinestretch=1.5,fontsize=\small]
    +AOR<"org">;
    -AOR<"org.Foo">;
\end{Verbatim}
%
Note that the flag for enabling or disabling operators is optional --- the
default flag (\verb|+|) for enabling operators improves readability but can be
omitted.

With regard to replacement definitions, there are two different possibilities:
Individual replacements can be added (\verb|+|) to an existing list or the
entire replacement list can be overridden (\verb|!|), where the latter
represents the default case if this optional flag is omitted. The following
example illustrates this feature, where the general definition of replacements
for the package \code{org} is extended for the class \code{Foo} but overriden
for the class \code{Bar}. The replacement lists that are effectively applied to 
the package and classes are given in comments. 
%
\begin{Verbatim}[baselinestretch=1.5,fontsize=\small]
     BIN(*)<"org">     -> {+,/};    // * -> {+,/}
    +BIN(*)<"org.Foo"> -> {%};      // * -> {+,/,%}
    !BIN(*)<"org.Bar"> -> {-};      // * -> {-}
\end{Verbatim}

\section{Operator groups} 
To prevent code duplication due to the repetition of equal definitions for 
several scopes (i.e., the same replacements or enabled mutation operators for
several packages, classes, or methods), \mml provides the possibility to
declare own operator groups.  Such a group may in turn contain any statement
that is valid in the context of the \mml, except for a call of another operator
group. An operator group is defined by means of a unique identifier and its
statements are enclosed by curly braces, as shown in the following example:

\begin{Verbatim}[baselinestretch=1.5,fontsize=\small]
    myGroup {
        BIN(*) -> {+,/};
        AOR;
    }
\end{Verbatim}

\section{Script examples}\label{sec:mml/examples}
Listing~\ref{list:mml/script} shows a simple example of a mutation script that
includes the following tasks:
\begin{itemize*}
\item Define specific replacement lists for \op{AOR} and \op{ROR}
\item Invoke the \op{AOR} and \op{ROR} operators on reduced lists
\item Invoke the \op{LVR} operator without restrictions
\end{itemize*}
\input{sections/mml/lstScript}

The more enhanced script in Listing~\ref{list:mml/tree} exploits the scoping
capabilities of \mml in line 8 and 13-20, and takes, additionally, advantage
of the possibility to define a variable in line 11 to avoid code duplication in
the subsequent scope declarations.  Both features are useful if only a certain
package, class, or method shall be mutated in a hierarchical software system.
\input{sections/mml/lstTree}

\noindent
Finally, the example in Listing~\ref{list:mml/call} visualizes the grouping
feature, which is useful if the same group of operations (replacement
definitions or mutation operator invocations) shall be applied to several
packages, classes, or methods. 
\input{sections/mml/lstCall}
