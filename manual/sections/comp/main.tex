\major extends the
\tool{OpenJDK} Java compiler and implements conditional
mutation as an optional transformation
of the abstract syntac tree (AST). In order to generate mutants, this
transformation has to be enabled by setting the compiler option \code{-XMutator}
--- if this flag is not set, then the compiler
works exactly as if it were unmodified.  The compile-time configuration of
conditional mutation and the necessary runtime driver are externalized to avoid
dependencies and to provide a non-invasive tool. This means that \major's
compiler can be used as a compiler replacement in any Java-based development
environment.

\section{Configuration} 
\major extends the non-standardized \<-X> options to avoid potential conflicts
with future releases of the Java compiler.  To use the mutation capabilities of
\major's compiler, the conditional mutation transformation has to be generally
enabled at compile-time using the compiler option \<-XMutator>.  
%
\major's compiler supports (1) compiler sub-options and (2) mutation scripts
(use \code{javac -X} to see a description of all configuration options):
%
\begin{itemize*}
\item[(1)] \code{javac -XMutator:<sub-options>}
\item[(2)] \code{javac -XMutator=<mml filename>}
\end{itemize*}
%
If the mutation step is enabled, \major's compiler prints the number of
generated mutants at the end of the compilation process and produces the log
file \file{mutants.log}, which contains detailed information about each
generated and embedded mutant.

\subsection{Compiler options}
\major's compiler provides wildcards and a list of valid sub-options, 
which correspond to the names of the available mutation operators.
For instance, the following three commands enable (1) all operators, using the
wildcard \op{ALL}, (2) all but one operator (\op{-LVR}),
and (3) a custom subset of operators, namely \op{AOR}, \op{ROR}, and \op{STD}:
%
\begin{itemize*}
\item[(1)] \code{javac -XMutator:ALL ...}
\item[(2)] \code{javac -XMutator:ALL,-LVR ...}
\item[(3)] \code{javac -XMutator:AOR,ROR,STD ...}
\end{itemize*}
%
Table~\ref{tab:conditional/op} summarizes the mutation operators that
are provided by \major's compiler.  
\input{sections/comp/tableOps}

\subsection{Mutation scripts}
Instead of using compiler options, \major's compiler can interpret 
mutation scripts written in its domain specific language \mml. These 
\mml scripts enable a detailed definition and a flexible application
of mutation operators.  For example, the replacement list for every 
operator in an operator group can be specified and mutations can be 
enabled or disabled for certain packages, classes, or methods. 
Within the following example, the mutation process is controlled by 
the definitions of the compiled script file \file{myscript.mml.bin}:
%
\begin{itemize*}
\item \<javac -XMutator="pathToFile/myscript.mml.bin" ...>
\end{itemize*}
%
Note that \major's compiler interprets pre-compiled script files. Use the script
compiler \mmlc to syntactically and semantically check, and compile a \mml
script file.  \major's domain specific language \mml is described in detail in
the subsequent Section~\ref{sec:mml}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Logging and exporting generated mutants}\label{sec:javac/log}
\subsection{Log file for generated mutants}
\label{sec:javac/log/file}
\major's compiler generates the log file \file{mutants.log}, which
provides detailed information about the generated mutants and uses a colon 
(\verb|:|) as separator. The log file contains one row per generated mutant, 
where each row in turn contains 7 columns with the following information:
\begin{enumerate*}
    \item Mutants' unique number (id)
    \item Name of the applied mutation operator
    \item Original operator symbol
    \item Replacement operator symbol
    \item Fully qualified name of the mutated method
    \item Line number in original source file
    \item Visualization of the applied transformation 
          (\code{from |==> to})
\end{enumerate*}
The following example gives the log entry for a ROR mutation that has the mutant
id 11 and is generated for the method \code{classify} (line number 18) of the
class \code{Triangle}:
\begin{small}
\begin{verbatim}
11:ROR:<=(int,int):<(int,int):Triangle@classify:18:a <= 0 |==> a < 0
\end{verbatim}
\end{small}

\subsection{Source-code export of generated mutants}
\label{sec:javac/log/export}
\major also supports the export of each generated mutant to an individual source
file --- this feature is disabled by default. If enabled, \major duplicates the 
original source file for each mutant, injects the mutant in the copy, and 
exports the resulting faulty copy. \major reads the following two properties
that control the export of generated mutants (default values are given in
parentheses):
\begin{itemize}
    \item \verb?-J-Dmajor.export.mutants=[true|false] (false)?
    \item \verb?-J-Dmajor.export.directory=<directory> (./mutants)?
    %-J-Dmajor.strict.flow=true
\end{itemize}
\major automatically creates the export directory and parent directories if
necessary.

Note that, if you are mutating a large code base, exporting all mutants to 
individual source files increases the compilation time and requires 
significantly more disk space than the log file.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Driver class}
\major references an external driver at runtime to gain access to a mutant 
identifier (\<M\_NO>) and a method that monitors mutation coverage (\<COVERED>). 
Listing \ref{list:comp/driver} shows an example of a simple driver class that
provides both the mutant identifier and the mutation coverage method. Note that
the mutant identifier and the coverage method must be implemented in a static
context to avoid any overhead caused by polymorphism and instantiation.  
\input{sections/comp/lstDriver}

The archive and source files of the default driver implementation is provided in 
the \dir{config} directory. Note that the driver class does \textbf{not} have to
be available on the classpath during compilation. \major does not try to
resolve the driver class at compile-time but instead assumes that the mutant
identifier and the coverage method will be provided by the driver class at runtime.
Thus, \major's compiler is non-invasive and the mutants can be generated without 
having a driver class available during compilation.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Integration into apache Ant's build.xml}\label{sec:javac/ant}
\major's compiler can be used standalone, but also in build systems,
such as Apache Ant. Consider, for example, the following 
\code{compile} target in an Apache Ant {build.xml} file:
\begin{small}
\begin{verbatim}

<target name="compile" depends="init" description="Compile">       
    <javac srcdir="src"                                            
          destdir="bin">
    </javac>                                                       
</target> 
\end{verbatim}
\end{small}

To use \major's compiler without any further changes to your environment, add 
the following 3 options to the \code{compile} target:
\begin{small}
\begin{verbatim}
<property name="mutOp" value=":NONE"/>
<target name="compile" depends="init" description="Compile">       
    <javac srcdir="src"                                            
          destdir="bin"                                            
      
             fork="yes"                                            
       executable="pathToMajor/bin/javac">                                      
       <compilerarg value="-XMutator${mutOp}"/>    
    </javac>                                                       
</target> 
\end{verbatim}
\end{small}
There is no need to duplicate the entire target since \major's compiler can also
be used for regular compilation. The following three commands illustrate how the
\code{compile} target shown above can be used to: (1) compile without mutation,
(2) compile with mutation using compiler options, and (3) compile with mutation
using a \mml script:
%
\begin{itemize*}
    \item[(1)] \code{ant compile}
    \item[(2)] \code{ant -DmutOp=":ALL" compile}
    \item[(3)] \code{ant -DmutOp="=pathToFile/myscript.mml.bin" compile}
\end{itemize*}
Note that the \code{mutOp} property provides a default value (\code{:NONE}) if
this property is not set on the command line.
